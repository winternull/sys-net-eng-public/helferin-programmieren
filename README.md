# Helferin Programmieren
Systems and networking helper program scripts of generic utility purpose.

## Code License
Unless otherwise noted in the script file(s), the programs in this repo are available via the BSD-3-Clause / BSDv3 license.

- License text: https://opensource.org/licenses/BSD-3-Clause
