#!/bin/bash
declare -r SWITCH_ADDRESS="192.168.0.3"
declare -r BASE_SWITCH_OID="1.3.6.1.4.1.318.1.1.4.4.2.1.3"
declare -r CABLE_MODEM_OUTLET="1"
declare -r ROUTER_OUTLET="2"
declare -r VM_HOST_OUTLET="3"
declare -r PRIMARY_NET_TEST_HOST="www.midcocomm.com"
declare -r SECODARY_NET_TEST_HOST="www.google.com"
declare -r VM_TEST_HOST="192.168.0.6"

power_on_device()
{
   echo "Powering on device $1"
   snmpset -v1 -cpublic $SWITCH_ADDRESS $BASE_SWITCH_OID.$1 i 1 >> /dev/null
}

power_off_device()
{
   echo "Powering off device $1"
   snmpset -v1 -cpublic $SWITCH_ADDRESS $BASE_SWITCH_OID.$1 i 2 >> /dev/null
}

reboot_device()
{
   echo "Rebooting device $1"
   snmpset -v1 -cpublic $SWITCH_ADDRESS $BASE_SWITCH_OID.$1 i 3 >> /dev/null
}

test_internet()
{
   ping -c1 -q $PRIMARY_NET_TEST_HOST >> /dev/null
   if [ $? -ne 0 ]; then
      echo "Primary internet host ping failure"
      ping -c1 -W10 -q $SECODARY_NET_TEST_HOST >> /dev/null
      if [ $? -ne 0 ]; then
         echo "Secondary internet host ping failure"
         return 0
      fi
   fi
   return 1
}

test_vm_host()
{
   ping -c1 -q $VM_TEST_HOST >> /dev/null
   if [ $? -ne 0 ]; then
      echo "VM Host first attempt failed"
      ping -c1 -W20 -q $VM_TEST_HOST >> /dev/null
      if [ $? -ne 0 ]; then
         echo "VM Host second attempt failed"
         return 0
      fi
   fi
   return 1
}

test_internet
if [ $? -eq 0 ]; then
   reboot_device $CABLE_MODEM_OUTLET
   reboot_device $ROUTER_OUTLET
fi

#test_vm_host
#if [ $? -eq 0 ]; then
#   reboot_device $VM_HOST_OUTLET
#fi