#!/bin/bash
# -------------------------------------------------------------------------- #
# File: hvac-all-on-off-toggle.sh
# Purpose: Toggles Tripp Lite SRCOOL12K HVAC + blower ON / OFF state
# Requires: SNMP "write+" enabled community on APC PDU for port toggle
# Date: 2022-09-27
# Author: same old bitch
# Repo URL: <PENDING>
# Version: 1.2.4
# License: Not the garbage GPL, use it if you want to
# -------------------------------------------------------------------------- #
# Reference links for potentially useful info:
# -------------------------------------------------------------------------- #
#  - https://fossies.org/linux/netxms/contrib/mibs/PowerNet-MIB.txt
#  - https://www.mail-archive.com/pacemaker@oss.clusterlabs.org/msg06951.html
#  - https://snmplabs.thola.io/snmpclitools/snmpset.html
#  - https://www.apc.com/us/en/faqs/FA156163/
#  - https://gist.github.com/bmatherly/3890485
# -------------------------------------------------------------------------- #
# Hardware Reference Notes
# -------------------------------------------------------------------------- #
#  - MIBs d/l: https://gist.githubusercontent.com/bmatherly/3890485/raw/ead189122dad0b6138ef985b8085ad288370d991/PowerNet-MIB.mib
#  - tested and validated on APC PDU AP7901 with firmware version info via cmd
#    snmpwalk -v1 -c private 172.16.40.252 1.3.6.1.2.1.1.1
#      SNMPv2-MIB::sysDescr.0 = STRING: APC Web/SNMP Management Card
#      (MB:v3.9.2 PF:v3.7.3
#      PN:apc_hw02_aos_373.bin
#      AF1:v3.7.3
#      AN1:apc_hw02_rpdu_373.bin
#      MN:AP7901 HR:B2
#      SN: 5A1222E01290 MD:05/22/2012)
# -------------------------------------------------------------------------- #
HVAC_USER="localadmin"
HVAC_PASS="localadmin"
HVAC_HOST="hvac-sfo000-srcool12k"
PDU_HOST="172.16.40.252"
PDU_PORT="4"
SNMP_VER="1"
SNMP_COMM="private"
SCRIPTNAME="hvac-all-on-off-toggle.sh"


# Notify syslog
# -------------------------------------------------------------------------- #
LOGDEST="systemd"
function logging() {
    MSG=$1
    NOW=`date +%Y%m%d-%H%M%S`
    if [ "${LOGDEST}" = "systemd" ]; then
        which systemd-cat >/dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "[DEBUG][$NOW] Could not locate systemd-cat binary in PATH. Using logger binary instead."
	    logger -p auth.notice "[${NOW}] HVAC enagement and blower toggled by hvac-all-on-off-toggle.sh"
        else
            echo "[NOTICE][${NOW}] ${MSG}" | systemd-cat -t ${SCRIPTNAME}
        fi
    elif [ "${LOGDEST}" = "stdout" ]; then
        echo "[${NOW}] ${MSG}"
    else
	echo "Failed to notify syslog or logger call" && exit 2;
    fi
}
# -------------------------------------------------------------------------- #

# Execute terminal command sequence for HVAC via SSH
# -------------------------------------------------------------------------- #
#  - This HVAC control unit lacks an alternate method (uses either via web-browser or ssh)
#  - The printf sequence navigates menu options iteratively via line breaks as enter-key presses
logging "Starting HVAC controller to toggle state"
(printf "\n1\n1\n3\n2\nY\nM\nQ\n" | SSHPASS="${HVAC_PASS}" sshpass -e ssh ${HVAC_USER}@${HVAC_HOST}) \
    || (logging "Failed to execute SSH command sequence to HVAC." && exit 2)

# Toggle power for blower fan via PDU port integer value, ON=1 OFF=2
# -------------------------------------------------------------------------- #
# Note, this SNMP set command toggles the selected PDU port number's integer state
#       if ARGV1 is missing then the script assumed you want the blower offline
#       if ARGV1 is explicitly set to "on" or "ON" then it will toggle the integer to online state
logging "Starting AC blower fan controller to toggle on/off state"

if [[ $1 = "on" ]] || [[ $1 = "ON" ]]; then
    # ref: note integer call 'i' w/ on value '1'
    snmpset -v${SNMP_VER} -c ${SNMP_COMM} ${PDU_HOST} .1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${PDU_PORT} i 1 \
	|| (logging "Failed to turn on blower via SNMP call to PDU" && exit 2)
else
    # ref: note integer call 'i' w/ off value '2'
    snmpset -v${SNMP_VER} -c ${SNMP_COMM} ${PDU_HOST} .1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${PDU_PORT} i 2 \
	|| (logging "Failed to turn on blower via SNMP call to PDU" && exit 2)
fi

logging "Sequence complete"
exit 0
# -------------------------------------------------------------------------- #
# SCRIPT END
# -------------------------------------------------------------------------- #
