#!/usr/bin/env bash
# ----------------------------------------------------------------------------|
# File: pdu-snmp-powercycler.sh
# Purpose: Toggles APC PDU port ON / OFF state via SNMP call
# Requires: SNMP "write+" enabled community on APC PDU for port toggle
# Date: 2024-01-27
# Author: em-winterschon
# Repo URL: <PENDING>
# Version: 2.1
# License: BSD 2-Clause
# ----------------------------------------------------------------------------|
SCRIPTNAME="pdu-snmp-powercycler.sh"

# Software Reference
# ----------------------------------------------------------------------------|
#  - https://fossies.org/linux/netxms/contrib/mibs/PowerNet-MIB.txt
#  - https://www.mail-archive.com/pacemaker@oss.clusterlabs.org/msg06951.html
#  - https://snmplabs.thola.io/snmpclitools/snmpset.html
#  - https://www.apc.com/us/en/faqs/FA156163/
#  - https://gist.github.com/bmatherly/3890485

# Hardware Reference
# ----------------------------------------------------------------------------|
#  - MIBs: https://gist.githubusercontent.com/bmatherly/3890485/raw/ead189122dad0b6138ef985b8085ad288370d991/PowerNet-MIB.mib
#  - tested and validated on APC PDU AP7901 with firmware version info via cmd
#    snmpwalk -v1 -c private 172.16.99.252 1.3.6.1.2.1.1.1
#      SNMPv2-MIB::sysDescr.0 = STRING: APC Web/SNMP Management Card
#      (MB:v3.9.2 PF:v3.7.3
#      PN:apc_hw02_aos_373.bin
#      AF1:v3.7.3
#      AN1:apc_hw02_rpdu_373.bin
#      MN:AP7901 HR:B2
#      SN: 5A1222E01290 MD:05/22/2012)
# ----------------------------------------------------------------------------|


# Write log data
# ----------------------------------------------------------------------------|
function logging() {
  SEV="${1}"
  MSG="${2}"
  NOW=$(date +%Y%m%d-%H%M%S)
  if [[ "${_debug}" -eq 1 ]]; then
    MSG="[DEBUG]: ${MSG}"
  fi

  logger -i -p local3.${SEV} "${MSG}"
  echo -e "[${NOW}] ${MSG}"
}


## Log Handlers
# ----------------------------------------------------------------------------|
function _log_debug() {
  MSG="${1}"
  SEV="debug"
  logging ${SEV} "${MSG}"
}

function _log_info() {
  MSG="${1}"
  SEV="info"
  logging ${SEV} "${MSG}"
}

function _log_notice() {
  MSG="${1}"
  SEV="notice"
  logging ${SEV} "${MSG}"
}

function _log_warn() {
  MSG="${1}"
  SEV="warn"
  logging ${SEV} "${MSG}"
}

function _log_error() {
  MSG="${1}"
  SEV="err"
  logging ${SEV} "${MSG}"
  exit $1;
}

function _log_crit() {
  MSG="${1}"
  SEV="crit"
  logging ${SEV} "${MSG}"
  exit $2;
}

function _log_alert() {
  MSG="${1}"
  SEV="alert"
  logging ${SEV} "${MSG}"
  exit $3;
}

function _log_emerg() {
  MSG="${1}"
  SEV="emerg"
  logging ${SEV} "${MSG}"
  exit $127;
}


## Prints banner info
# ----------------------------------------------------------------------------|
function _banner() {
    cat<<EOF
----------------------------------------------------------------------------- #
File: pdu-snmp-powercycler.sh
Purpose: Toggles APC PDU port ON / OFF state via SNMP call
Requires: SNMP "write+" enabled community on APC PDU for port toggle
Date: 2024-01-05
Author: em-winterschon
Repo URL: <PENDING>
Version: 1.4
License: BSD 2-Clause
----------------------------------------------------------------------------- #
EOF
}

## Usage
# ----------------------------------------------------------------------------|
function _self.help() {
    _banner
    cat<<EOF
Usage:   ${SCRIPT} [options]
|-----------------------------------------------------------------------------|
 -h, --help        print this help text                                       #
 -d, --debug       enable verbose debug mode                                  #
 -x, --quiet       operate with less verbosity                                #
                                                                              #
 PDU Power Actions                                                            #
 ------------------                                                           #
 --pwr-on          set PDU port action to 'power on'                          #
 --pwr-off         set PDU port action to 'power off'                         #
 --pwr-cycle       set PDU port action to 'power cycle'                       #
 -p, --outlet-num  set integer value of PDU outlet to operate on              #
                                                                              #
 Future Options                                                               #
 ------------------                                                           #
 -n, --outlet-str  set name of PDU outlet to operate on                       #
 -c, --config      set filename for configuration data                        #
 -q, --query       print list of configured PDU port devs                     #
 --logmode         select syslog in addition to stdout                        #
 --logfile         specify an optional logging file for stdout / stderr       #
                                                                              #
 SNMP Connection Parameters                                                   #
 ------------------                                                           #
 --snmpset         set binary full-path for 'snmpset'      default: PATH      #
 --snmpwalk        set binary full-path for 'snmpwalk'     default: PATH      #
 --snmp-host       set SNMP host value (required)          default: none      #
 --snmp-port       set SNMP port value (optional)          default: 161       #
 --snmp-proto      set SNMP protocol version: udp, tcp     default: udp       #
 --snmp-comm-ro    set SNMP read-only community            default: public    #
 --snmp-comm-wr    set SNMP write community,               default: private   #
 --snmp-version    set SNMP version, options: 1, 2, 3      default: 1         #
                                                                              #
 Logging Options                                                              #
 ------------------                                                           #
 -w, --wait        alternate pause timing between commands  [default: 5]      #
|-----------------------------------------------------------------------------|

EOF
}


## Check ARGV array
# ----------------------------------------------------------------------------|
if [ $# -eq 0 ]; then
  _self.help;
  exit 127;
else
  if [ $# -eq 1 ]; then
    if [ ${1} = "-h" ]; then
      _self.help; exit 127;
    elif [ ${1} = "--help" ]; then
      _self.help; exit 127;
    else
      skip="no";
    fi
  fi
fi


## Set Base States
# ----------------------------------------------------------------------------|
_help=0
_debug=0
_quiet=0
_query=0
_null=0

_dev_pwr_on=0
_dev_pwr_off=0
_dev_pwr_cyc=0

_conf_file=""
_outlet_num=""
_outlet_str=""

_snmpset=$(which snmpset 2>&1;)
_snmpwalk=$(which snmpwalk 2>&1;)
_snmp_host="172.16.99.252"
_snmp_port="161"
_snmp_proto="udp"
_snmp_comm_ro="rfc1918.read"
_snmp_comm_wr="rfc1918.write"
_snmp_version="1"

_log_mode="stdout"  # stdout || syslog
_log_file=""
_timer_wait=5


## Check/Set GetOpts
# ----------------------------------------------------------------------------|
if [[ ${_null} -eq 0 ]]; then
  while [[ $# -gt 0 ]]; do
    case $1 in
      # Short Opts
      -h|--help)      _help=1 ;;
      -d|--debug)     _debug=1 ;;
      -x|--quiet)     _quiet=1 ;;
      -q|--query)     _query=1 ;;

      # Power states
      --pwr-on)       _dev_pwr_on=1   ;;
      --pwr-off)      _dev_pwr_off=1  ;;
      --pwr-cycle)    _dev_pwr_cyc=1  ;;

      # Long Opts
      -c|--config)      _conf_file="$2"    ; shift;;
      -p|--outlet-num)  _outlet_num="$2"   ; shift;;
      -n|--outlet-str)  _outlet_str="$2"   ; shift;;

      # SNMP vars
      --snmpset)        _snmpset="$2"      ; shift;;
      --snmpwalk)       _snmpwalk="$2"     ; shift;;
      --snmp-host)      _snmp_host="$2"    ; shift;;
      --snmp-port)      _snmp_port="$2"    ; shift;;
      --snmp-proto)     _snmp_proto="$2"   ; shift;;
      --snmp-comm-ro)   _snmp_comm_ro="$2" ; shift;;
      --snmp-comm-wr)   _snmp_comm_wr="$2" ; shift;;
      --snmp-version)   _snmp_version="$2" ; shift;;

      # Logging
      --syslog)         _log_mode="$2"     ; shift;;
      --logfile)        _log_file="$2"     ; shift;;

      # Timers
      -w|--wait)        _timer_wait="$2"   ; shift;;

      # Cleanup
      (--) shift; break;;
      (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
      (*) break;;
    esac
    shift
  done
fi


# Process Actions
# ----------------------------------------------------------------------------|
#  Function: toggle the selected PDU port number's integer state
#  Action: PDU port integer value, ON=1 OFF=2
# ----------------------------------------------------------------------------|
if [[ "${_debug}" -eq 1 ]]; then
  set -x
fi

_log_info "Reading controller request to toggle on/off power port state..."

# Check snmp port integer
if ! [[ "${_snmp_port}" =~ ^[0-9]+$ ]]; then
  _log_crit "SNMP port requires integer range 1-65535"
fi

# Check snmp protocol state
_snmp_proto=$(echo ${_snmp_proto} | awk '{print tolower($0)}')
if ! ([[ "${_snmp_proto}" = "udp" ]] || [[ "${_snmp_proto}" = "tcp" ]]); then
  _log_crit "SNMP protocol must be either udp or tcp"
fi

# Build connection strings
__cmdstr="${_snmpset} -v${_snmp_version} -c ${_snmp_comm_wr} ${_snmp_proto}:${_snmp_host}:${_snmp_port}"
__query="${_snmpwalk} -v${_snmp_version} -c ${_snmp_comm_wr} ${_snmp_proto}:${_snmp_host}:${_snmp_port}"


# PDU State Functions
# ----------------------------------------------------------------------------|
function _pdu_func_port_enable() {
  # ref: integer call 'i' --> ON value '1'
  ${__cmdstr} .1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${_outlet_num} i 1 \
    || _log_crit "Failed to turn ON port via SNMP call to PDU"
}

function _pdu_func_port_disable() {
  # ref: integer call 'i --> OFF value '2'
  ${__cmdstr} .1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.${_outlet_num} i 2 \
    || _log_crit "Failed to turn OFF port via SNMP call to PDU"
}

function _pdu_func_query_devices() {
  ${__query} .1.3.6.1.4.1.318.1.1.12.3.3.1.1.2 \
    || _log_crit "Failed to query devices via SNMP call to PDU"
}

# PDU State Toggles
# ----------------------------------------------------------------------------|
# exec: on
if ([[ $_dev_pwr_on -eq 1 ]] && \
    [[ $_dev_pwr_off -eq 0 ]]); then
  _log_info "Action request: set port power ON"
  _pdu_func_port_enable

# exec: off
elif ([[ $_dev_pwr_off -eq 1 ]] && \
      [[ $_dev_pwr_on -eq 0 ]]); then
  _log_info "Action request: set port power OFF"
  _pdu_func_port_disable

# exec: cycle
elif ([[ $_dev_pwr_cyc -eq 1 ]] && \
      [[ $_dev_pwr_on -eq 0 ]] && \
      [[ $_dev_pwr_off -eq 0 ]]); then
  _log_info "Action request: set port power CYCLE"
  _pdu_func_port_disable
  _log_info "pausing ${_timer_wait} seconds ..."
  sleep _timer_wait
  _pdu_func_port_enable
  _log_info "Action response: power cycle command sequence complete"

# exec: query
elif ([[ $_query -eq 1 ]] && \
      [[ $_dev_pwr_cyc -eq 0 ]] && \
      [[ $_dev_pwr_on -eq 0 ]] && \
      [[ $_dev_pwr_off -eq 0 ]]); then
  _log_info "Action request: display port devices via QUERY"
  _pdu_func_query_devices

else
 _log_error "Power state request conflict or action missing from ARGs, exiting.";

fi

# End Stage
# ----------------------------------------------------------------------------|
_log_info "Power action sequence complete"
set +x
exit 0
# ----------------------------------------------------------------------------|

