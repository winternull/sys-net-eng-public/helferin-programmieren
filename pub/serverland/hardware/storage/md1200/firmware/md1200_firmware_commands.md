# show versions and image regions
```
_ver
********************* Bench Build **********************
Build Owner   : GitHub
Build Number  : 000
Build Version : 106
Build Date    : Mon Aug 24 12:28:59 2015
FPGA Revision  : A9
Board Revision : 8
CPLD Revision  : 7
**************Firmware Image Information****************
          Image Region 0 (Always Boot)
             Revision         : 0.0.63.0
             Dell Version     : 1.01
             Total Image Size : 0003d89c [252060]
             Fast Boot        : Yes
             Image Address    : 0x14000000
             RegionOffset     : 0x00000000
             RegionSize       : 0x00080000
             RegionType       : 0x00000000
          Image Region 1
             Revision         : 0.0.63.0
             Dell Version     : 1.01
             Total Image Size : 0003d89c [252060]
             Fast Boot        : Yes
             Image Address    : 0x14080000
             RegionOffset     : 0x00080000
             RegionSize       : 0x00080000
             RegionType       : 0x00000001
Active--->Image Region 2
             Revision         : 0.0.63.0
             Dell Version     : 1.06
             Total Image Size : 000414fc [267516]
             Fast Boot        : Yes
             Image Address    : 0x14100000
             RegionOffset     : 0x00100000
             RegionSize       : 0x00080000
             RegionType       : 0x00000002
********************************************************

BlueDress.106.000 >
```

# Erase Image Region 2
```
_erase 2
```

# Output Region 1 to serial output
Command help: `_flashdump     Dump Flash: flashdump <offset> <length>`

```
_flashdump 0x00080000 0x00100000
```

# convert minicom serial capture HEX to BIN
```
xxd -r -p <capture file> <output file>.bin
```
