#!/bin/bash
#---------------------------------------------------------------------------------------#
## File: md1200-srvc-exec.sh
## Purpose: executes MD1200 service commands
## Date: 2021-09-24
## Version: 1.0.0
## Requires: systemd service md1200-srvc-exec.service, timer md1200-srvc-exec.timer
## Optional: /etc/logrotate.d/md1200-srvc-exec
## Permissions: script MUST have access to digibox / RS232 device connected to MD1200
#---------------------------------------------------------------------------------------#
#DEVICES="ttyUSB0"
DEVICES="ttyUSB0 ttyUSB1 ttyUSB2 ttyUSB3"
LOGFILE="/var/log/md1200-srvc-exec.log"
#---------------------------------------------------------------------------------------#

DATE=`date +"%Y%m%d%H%M"`
echo -e "${DATE} OP-START: md1200-srvc-exec.sh" >> ${LOGFILE}

for dev in ${DEVICES}; do
    echo "setting ${dev} stty properties: 38400 cs8 -ixon raw" >> ${LOGFILE}
    stty -F /dev/${dev} speed 38400 cs8 -ixon raw
    echo "sending fan control '_shutup 20' to ${dev}" >> ${LOGFILE}
    echo -ne "_shutup 20\n\r" > /dev/${dev}
    #echo -ne "set_speed 20\n\r" > /dev/${dev}
done

echo -e "${DATE} OP-END: md1200-srvc-exec.sh" >> ${LOGFILE}


