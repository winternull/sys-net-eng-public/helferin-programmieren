#!/bin/sh
export XDG_BASED="/home/${USER}"
export XDG_CONFIG_HOME="${XDG_BASED}/.config"
export XDG_STATE_HOME="${XDG_BASED}/.local/state"

APP_BIN="$(which ungoogled-chromium)"  #/usr/local/bin/ungoogled-chromium"
APP_PFL="Profile 2"
APP_URI="soundcloud.chromium"
APP_URL="https://soundcloud.com/winterschon"
APP_OPT="--user-dir=${XDG_STATE_HOME}/${APP_URI} --profile-directory='${APP_PFL}' --new-window ${APP_URL}"
#APP_OPT="--app=${APP_URL} --user-dir=${XDG_STATE_HOME}/${APP_URI} --profile-directory='${APP_PFL}' --new-window"
/bin/rm ${XDG_CONFIG_HOME}/chromium/SingletonCookie >/dev/null 2>&1;
/bin/rm ${XDG_CONFIG_HOME}/chromium/SingletonLock >/dev/null 2>&1;
/bin/rm ${XDG_CONFIG_HOME}/chromium/SingletonSocket >/dev/null 2>&1;

mkdir -p "${XDG_STATE_HOME}/${APP_URI}"

echo "${APP_BIN} ${APP_OPT}"
${APP_BIN} ${APP_OPT}
