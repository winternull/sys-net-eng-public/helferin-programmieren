#!/bin/sh

NAME_APP="bitwarden"
NAME_SSH="localhost"
NAME_BIN="/usr/local/bin/iridium"
NAME_ORG="de.iridium"
FLAG_URL="--app=https://vault.bitwarden.com"
FLAG_OPT="--new-window \
  --disable-gpu \
  --disable-background-networking \
  --disable-webgl \
  --restore-last-session \
  --disable-dev-shm-usage \
  --disable-low-end-device-mode \
  --disable-software-rasterizer \
  --enable-background-thread-pool \
  --force-dark-mode \
  --log-file ~/.local/logs/${NAME_ORG}.debug.${NAME_APP}.log \
  --memlog ~/.local/logs/${NAME_ORG}.memory.${NAME_APP}.log \
  --no-default-browser-check \
  --no-experiments \
  --no-pings \
  --noerrdialogs"
#FLAG_OPT="--new-window --disable-gpu --disable-background-networking --disable-webgl --restore-last-session --disable-dev-shm-usage --disable-low-end-device-mode --disable-software-rasterizer --enable-background-thread-pool --force-dark-mode --log-file ~/.local/logs/${NAME_ORG}.debug.${NAME_APP}.log --memlog ~/.local/logs/${NAME_ORG}.memory.${NAME_APP}.log --no-default-browser-check --no-experiments --no-pings --noerrdialogs"

#ssh -X ${NAME_SSH} "${NAME_BIN} ${FLAG_URL} ${FLAG_OPT}"
ssh -X ${NAME_SSH} "${NAME_BIN} ${FLAG_URL} ${FLAG_OPT}"

