#!/bin/sh
XDG_BASED="/home/${USER}"
XDG_CONFIG_HOME="${XDG_BASED}/.config"
XDG_STATE_HOME="${XDG_BASED}/.local/state"

X11_SCALE="0.8"

APP_BIN="/usr/local/bin/ungoogled-chromium"
APP_URI="based.ungoogled_chromium"
APP_URL="https://search.brave.com"
APP_OPT="--force-device-scale-factor=${X11_SCALE} --user-dir=${XDG_STATE_HOME}/${APP_URI} --new-window ${APP_URL}"
/bin/rm ${XDG_CONFIG_HOME}/ungoogled-chromium/SingletonCookie >/dev/null 2>&1;
/bin/rm ${XDG_CONFIG_HOME}/ungoogled-chromium/SingletonLock >/dev/null 2>&1;
/bin/rm ${XDG_CONFIG_HOME}/ungoogled-chromium/SingletonSocket >/dev/null 2>&1;

mkdir -p "${XDG_STATE_HOME}/${APP_URI}"

echo "${APP_BIN} ${APP_OPT}"
${APP_BIN} ${APP_OPT}
