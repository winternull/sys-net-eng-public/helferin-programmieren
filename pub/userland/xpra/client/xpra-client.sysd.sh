#!/usr/bin/env sh

xpra attach \
  tcp://172.16.99.94:14500 \
  --auth=none \
  --forward-xdg-open=true \
  --modal-windows=true \
  --open-command=/usr/local/bin/xdg-open \
  --opengl=yes \
  --compressors=brotli,lz4 \
  --video-encoders=nvenc \
  --video-decoders=nvdec \
  --audio=no \
  --mdns=no \
  --dpi=60 \
  --desktop-scaling=off \
  --video-scaling=off \
  --refresh-rate=60 \
  --headerbar=force \
  --border=red,1 \
  --printing=no \
  --sharing=no \
  --log-dir=/var/log/xpra \
  --log-file=xpra-client.${HOSTNAME}.log \
  --sync-xvfb=30
