#!/usr/bin/env sh

#xpra attach ws://plastik.pink.systems:11000 \
xpra attach \
  ws://172.16.99.94:11000 \
  --forward-xdg-open=true \
  --modal-windows=true \
  --open-command=/usr/local/bin/xdg-open \
  --compressors=brotli,lz4 \
  --video-encoders=nvenc \
  --video-decoders=nvdec \
  --audio=no \
  --mdns=no \
  --dpi=160 \
  --desktop-scaling=on \
  --video-scaling=on \
  --resize-display=off \
  --printing=no \
  --sharing=yes \
  --encoding=auto \
  --clipboard=yes \
  --clipboard-direction=both \
  --file-transfer=on \
  --keyboard-layout=pc105 \
  --log-dir=/var/log/xpra \
  --log-file=xpra-client.${HOSTNAME}.log

# Removed options
#  --refresh-rate=60 \
#  --sync-xvfb=30


#| XPRA Server Settings Required |
#|_______________________________|
# #!/usr/bin/env bash
# /usr/bin/xpra clean-sockets
# /usr/bin/xpra start \
# --opengl=yes \
#  --uid=1024 \
#  --gid=10 \
#  --start=xterm \
#  --systemd-run=no \
#  --bind-ws=0.0.0.0:11000 \
#  --compressors=all \
#  --exit-ssh=no \
#  --daemon=no \
#  --html=off \
#  --log-dir=/var/log/xpra \
#  --log-file=xpra-server.${HOSTNAME}.log \
#  --encoding=auto \
#  --video-encoders=all \
#  --video-decoders=all \
#  --windows=yes \
#  --clipboard=yes \
#  --readonly=no \
#  --clipboard-direction=both \
#  --file-transfer=off \
#  --forward-xdg-open=on \
#  --resize-display=no \
#  --mdns=no \
#  --bell=no \
#  --webcam=no \
#  --printing=no \
#  --audio=no \
#  --pulseaudio=no \
#  --dbus-proxy=yes \
#  --mousewheel=on \
#  --keyboard-sync=yes \
#  --keyboard-layout=pc86 \
#  --speaker=off \
#  --microphone=off \
#  --enable-pings
#|_______________________________|

