#!/usr/bin/env sh
systemctl --no-pager -l --user start xpra-server.service
sleep 2
systemctl --no-pager -l --user status xpra-server.service
