#!/usr/bin/env bash
/usr/bin/xpra clean-sockets
/usr/bin/xpra start \
  --uid=1024 \
  --gid=10 \
  --systemd-run=no \
  --bind-ws=0.0.0.0:11000 \
  --compressors=all \
  --exit-ssh=no \
  --daemon=no \
  --html=off \
  --log-dir=/var/log/xpra \
  --log-file=xpra-server.${HOSTNAME}.log \
  --encoding=auto \
  --video-encoders=all \
  --video-decoders=all \
  --windows=yes \
  --clipboard=yes \
  --readonly=no \
  --clipboard-direction=both \
  --file-transfer=off \
  --forward-xdg-open=on \
  --resize-display=yes \
  --mdns=no \
  --bell=no \
  --webcam=no \
  --printing=no \
  --audio=yes \
  --pulseaudio=no \
  --enable-pings


